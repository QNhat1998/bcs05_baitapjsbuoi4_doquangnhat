let getElement = id => document.getElementById(id);

let innerContent = (id, content) => getElement(id).innerText = content

let kiemtraCanh = (canh1, canh2, canh3) => canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh3 + canh2 > canh1;

let pitago = (canh1, canh2) => Math.sqrt(Math.pow(canh1, 2) + Math.pow(canh2, 2))

let location4 = (X, x, Y, y) => Math.sqrt(Math.pow(X - x, 2) + Math.pow(Y - y, 2))

let kiemTraThangNam = (month, year) => {
    return (month >= 1 && month <= 12) && year >= 1920
}

checkDate = () => {
    let day = getElement('day').value,
        month = getElement('month').value,
        year = getElement('year').value

    let date = new Date(`${month}/${day}/${year}`);

    return date;
}

getTextNumber = (number, position) => {
    let textNum = '';
    switch (number) {
        case 1:
            textNum = position == 't' ? "Một trăm" : position == "c" ? "mười" : "một"
            break;
        case 2:
            textNum = position == 't' ? "Hai trăm" : position == "c" ? "hai mươi" : "hai"
            break;
        case 3:
            textNum = position == 't' ? "Ba trăm" : position == "c" ? "ba mươi" : "ba"
            break;
        case 4:
            textNum = position == 't' ? "Bốn trăm" : position == "c" ? "bốn mươi" : "bốn"
            break;
        case 5:
            textNum = position == 't' ? "Năm trăm" : position == "c" ? "năm mươi" : "năm"
            break;
        case 6:
            textNum = position == 't' ? "Sáu trăm" : position == "c" ? "sáu mươi" : "sáu"
            break;
        case 7:
            textNum = position == 't' ? "Bảy trăm" : position == "c" ? "bảy mươi" : "bảy"
            break;
        case 8:
            textNum = position == 't' ? "Tám trăm" : position == "c" ? "tám mươi" : "tám"
            break;
        case 9:
            textNum = position == 't' ? "Chín trăm" : position == "c" ? "chín mươi" : "chín"
            break;
        case 0:
            textNum = position == 't' ? "" : position == "c" ? "lẻ" : ""

            break;
    }
    return textNum
}

getElement('btn-sort').addEventListener("click", () => {
    let num1 = getElement('soThuNhat').value,
        num2 = getElement('soThuHai').value,
        num3 = getElement('soThuBa').value

    if (num1 > num2) {
        [num1, num2] = [num2, num1]
    }
    if (num1 > num3) {
        [num1, num3] = [num3, num1]
    }
    if (num2 > num3) {
        [num2, num3] = [num3, num2]
    }
    innerContent('result-sort', `${num1},${num2},${num3}`)
})

getElement('btn-hello').addEventListener("click", () => {
    let member = getElement('parent').value
    let hi = ''
    switch (member) {
        case 'B':
            hi = "Bố";
            break;
        case 'M':
            hi = "Mẹ";
            break;
        case 'A':
            hi = "Anh trai";
            break;
        case 'E':
            hi = "Em gái";
            break;
    }
    if (hi.trim() === '') alert('Chưa chọn đối tượng')
    else innerContent('result-hello', `Xin chào ${hi}`)

})

getElement('btn-count').addEventListener("click", () => {
    let chan = 0,
        num1 = getElement('num1').value,
        num2 = getElement('num2').value,
        num3 = getElement('num3').value

    chan = (num1 % 2 == 0) + (num2 % 2 == 0) + (num3 % 2 == 0)

    innerContent('result-count', `Có ${chan} số chẵn và ${3 - chan} số lẻ`)
})

getElement('btn-triangle').addEventListener("click", () => {
    let canh1 = Number(getElement('canh1').value),
        canh2 = Number(getElement('canh2').value),
        canh3 = Number(getElement('canh3').value),
        str = ''

    if (kiemtraCanh(num1, num2, num3)) {
        canh1 == canh2 && canh2 == canh3 ? str = 'Tam giác đều'
            : canh1 == canh2 || canh1 == canh3 || canh2 == canh3 ? str = 'Tam giác cân'
                : canh1 == pitago(canh2, canh3) || canh2 == pitago(canh1, canh3) || canh3 == pitago(canh2, canh1) ? str = 'Tam giác vuông'
                    : str = 'Hình tam giác khác'
    } else {
        alert('Dữ liệu nhập sai')
    }
    innerContent('result-triangle', str)
})

getElement('btn-tomorrow').addEventListener("click", () => {

    let date = checkDate()

    if (date == 'Invalid Date') {
        alert('Dữ liệu nhập sai')
    } else {
        let tomorrow = new Date(date);
        tomorrow.setDate(tomorrow.getDate() + 1)
        innerContent('result-day', tomorrow.toLocaleDateString('en-GB'))
    }

})

getElement('btn-yesterday').addEventListener("click", () => {
    let date = checkDate()

    if (date == 'Invalid Date') {
        alert('Dữ liệu nhập sai')
    } else {
        let yesterday = new Date(date);
        yesterday.setDate(yesterday.getDate() - 1)
        innerContent('result-day', yesterday.toLocaleDateString('en-GB'))
    }
})

getElement('btn-day').addEventListener("click", () => {

    let month = Number(getElement('month2').value),
        year = Number(getElement('year2').value),
        day = 0, n
    if (kiemTraThangNam(month, year)) {
        n = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;
            case 2:
                day = n ? 29 : 28;
                break;
        }
        innerContent('result-dayNumber', `Tháng ${month} có ${day} ngày`)
    } else {
        alert('Dữ liệu sai');
    }
})

getElement('btn-number').addEventListener("click", () => {
    let number = Number(getElement('number').value), t, c, dv
    if (!(number < 100 || number > 1000)) {
        t = Math.floor(number / 100)
        c = Math.floor((number % 100) / 10)
        dv = (number % 100) % 10
        innerContent('result-number', `${getTextNumber(t, 't')} ${getTextNumber(c, 'c')} ${getTextNumber(dv, 'dv')}`)
    } else {
        alert("Đọc kĩ nội dung.");
    }
})

getElement('btn-location').addEventListener("click", () => {
    let name1 = getElement('name1').value,
        x1 = Number(getElement('x1').value),
        y1 = Number(getElement('y1').value),
        name2 = getElement('name2').value,
        x2 = Number(getElement('x2').value),
        y2 = Number(getElement('y2').value),
        name3 = getElement('name3').value,
        x3 = Number(getElement('x3').value),
        y3 = Number(getElement('y3').value),
        x4 = Number(getElement('x4').value),
        y4 = Number(getElement('y4').value),
        location1, location2, location3

    location1 = location4(x4, x1, y4, y1),
        location2 = location4(x4, x2, y4, y2),
        location3 = location4(x4, x3, y4, y3)

    let name = location1 > location2 && location1 > location3 ? name1
        : location2 > location1 && location2 > location3 ? name2
            : name3
    innerContent('result-location', `Sinh viên xa trường nhất là: ${name}`)
})
